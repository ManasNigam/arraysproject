const mapFunctionFromScratch = function (givenArray, cb) {

    const newArray = [];

    for (let count = 0; count < givenArray.length; count++) {
        newArray[count] = cb(givenArray[count], count, givenArray);
    }

    return newArray;
}

module.exports = mapFunctionFromScratch