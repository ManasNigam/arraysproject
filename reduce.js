const myReduce = (elements, cb, startingValue) => {

  let index = 0;
  if (elements.length === 0 && startingValue === undefined) {
    throw new Error('Reduce of empty array with no initial value');
  }



  if (startingValue === undefined) {
    startingValue = elements[0]
    index = 1
  }

  for (; index < elements.length; index++) {
    startingValue = cb(startingValue, elements[index], index, elements);
  }

  return startingValue
}


module.exports = myReduce

