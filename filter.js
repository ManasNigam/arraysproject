const result = (elements, cb) => {

    let resultArray = []
    for (let counter = 0; counter < elements.length; counter++) {
        if (cb(elements[counter], counter, elements) === true) {
            resultArray.push(elements[counter])
        }
    }
    return resultArray
}
module.exports = result


// The callback function does not get access to the index as the second parameter to the callback function.