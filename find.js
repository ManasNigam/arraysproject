const result = (elements, cb) => {
    for (let i = 0; i < elements.length; i++) {
        if (cb(elements[i]) === true) {
            return elements[i]
        }
    }
    return undefined

}

module.exports = result