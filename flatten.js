const filter = require("./filter")
const flattenArray = (givenArray, depth = 1) => {
    let arr = [];
    givenArray = filter(givenArray, function (args) {
        if (args === undefined) {
            return false
        }
        return true;
    })


    if (depth > 0) {

        for (let index = 0; index < givenArray.length; index++) {

            if (Array.isArray(givenArray[index])) {

                arr = arr.concat(flattenArray(givenArray[index], depth - 1))
            }
            else {
                arr.push(givenArray[index])
            }

        }

        return arr;
    }

    return givenArray
}

module.exports = flattenArray